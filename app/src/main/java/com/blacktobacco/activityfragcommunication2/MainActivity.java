package com.blacktobacco.activityfragcommunication2;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.otto.ThreadEnforcer;

public class MainActivity extends AppCompatActivity {

  @Override protected void onStart() {
    super.onStart();

    SharedData.bus().register(this);
  }

  @Override protected void onStop() {
    super.onStop();
    SharedData.bus().unregister(this);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Fragment newFragment = OneFragment.newInstance(null, null);
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    ft.add(R.id.theFragment, newFragment).commit();

  }

  @Subscribe
  public void onLoginAction(OneFragment.LogInEvent event) {
    Log.d("Test", "LogIn action name " + event.name + " pass " + event.pass);
  }
}
