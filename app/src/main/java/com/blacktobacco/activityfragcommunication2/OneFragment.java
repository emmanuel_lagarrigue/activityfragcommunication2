package com.blacktobacco.activityfragcommunication2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class OneFragment extends Fragment {

  // the fragment initialization parameters
  private static final String ARG_NAME = "name";
  private static final String ARG_PASS = "pass";

  private String mName;
  private String mPass;

  private EditText nameET;
  private EditText passET;

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param name Parameter 1.
   * @param pass Parameter 2.
   * @return A new instance of fragment OneFragment.
   */
  public static OneFragment newInstance(String name, String pass) {
    OneFragment fragment = new OneFragment();
    Bundle args = new Bundle();
    args.putString(ARG_NAME, name);
    args.putString(ARG_PASS, pass);
    fragment.setArguments(args);
    return fragment;
  }

  public OneFragment() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      if (getArguments().containsKey(ARG_NAME)) {
        mName = getArguments().getString(ARG_NAME);
      }
      if (getArguments().containsKey(ARG_PASS)) {
        mPass = getArguments().getString(ARG_PASS);
      }
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_one, container, false);

    nameET = (EditText) v.findViewById(R.id.nameET);
    passET = (EditText) v.findViewById(R.id.passET);

    if(mName != null) {
      nameET.setText(mName);
    }
    if(mPass != null) {
      passET.setText(mPass);
    }

    v.findViewById(R.id.logInBtn).setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        loginAction();
      }
    });

    return v;
  }


  public void loginAction() {
    SharedData.bus().post(new LogInEvent(nameET.getText().toString(), passET.getText().toString()));
  }

  public class LogInEvent {
    public String name;
    public String pass;

    public LogInEvent(String name, String pass) {
      this.name = name;
      this.pass = pass;
    }
  }

}
